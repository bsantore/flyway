USE flyway_sandbox;

CREATE TABLE people
(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(42) NOT NULL,
    lastname VARCHAR(42) NOT NULL
)