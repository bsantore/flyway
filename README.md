# Flyway Sandbox
This is a sandbox environment to play with Flyway. It isn't, and doesn't attempt to be a production application.

# Requirements

- Docker
- Docker Compose

# Commands
Start the sandbox

`docker-compose -f docker-compose.yml up`

This will spin up a MySql instance with an ephemeral volume.

There is also a container running with `flyway` installed. Tailing `/dev/null` isn't a best practice, but it helps provide easy access to the `flyway` cli.

`docker exec flyway-sand_migrations_1 flyway migrate`

Some sample migrations have been provided. Since this is a sandbox, you can add your own migrations to `migrations`. They will be mounted as a volume where you can continue to run migrations in the flyway container.
